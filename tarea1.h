#pragma once
#include <iostream>
#include <string>
#include "math.h"
#include <vector>
#include <fstream>

using namespace std;

//En este archivo se encuentran los encabezados de las funciones implementadas

//Funciones que calculan los indices de cada predictor
int Bimodal(long int PC, int BHTmax);
int Global(long int PC, int BHTmax, int GlobalResgisterMAX, int &RegistroGlobal);
int Private(long int PC, vector<int> &Table, int &indiceTable, int BHTmax, int PrivateResgisterMAX);

//Función que crea el predictor
int* CrearPredictor(int size);

//Función que crea la tabla privada
vector<int> PrivateTable(int BHT);

//Funciones que realizan la lógica necesaria de cada predictor
void PredictorBimodal(int predictor[], string state, int indice, int &NumBranch, int &CorrectTaken, int &IncorrectTaken, int &CorrectNotTaken, int &IncorrectNotTaken);
bool PredictorGlobal(int predictor[], int &RegistroGlobal, string state, int indice, int &NumBranch, int &CorrectTaken, int &IncorrectTaken, int &CorrectNotTaken, int &IncorrectNotTaken);
bool PredictorPrivado(int predictor[], vector<int> &Table, string state, int indice, int &indiceTable, int &NumBranch, int &CorrectTaken, int &IncorrectTaken, int &CorrectNotTaken, int &IncorrectNotTaken);
void Tournament(int metapredictor[], bool privado, bool global, int indice);

//Función para crear el archivo
void CrearArchivo(ofstream &archivo, int predictor[], int indice, long int PC, string state);

//Función que calcula el porcentaje de predicciones correctas
float Percentage(int &NumBranch, int &CorrectTaken, int &CorrectNotTaken);

//Función que imprime en consola los resultados
void DisplayInfo(int tipo, int BHT, int GlobalRegister, int PrivateRegister, int NumBranch, int CorrectTaken, int IncorrectTaken, int CorrectNotTaken, int IncorrectNotTaken, float PercentageCorrect);


