# Tarea1 - Estructuras de compus 2
## Miembros
- Jorge Isaac Fallas Mejía B62562
## Descripción
El programa implementa algoritmos para distintos predictores de saltos: **bimodal**, **global**, **privado**, **torneo**. Dichos saltos se obtienen de un archivo comprimido, el cual sirve de entrada para el programa. Como resultado de implementar estos algoritmos se obtiene lo siguiente:
- La cantidad de saltos realizados. 
- El número de predicciones correctas para los resultados tomados. 
- El número de predicciones incorrectas para saltos tomados. 
- El número de predicciones correctas para saltos no tomados. 
- El número de predicciones incorrectas de saltos no tomados. 
- El porcentaje de predicciones correctas. 

Además se implementa un parámetro para realizar un archivo con las predicciones de los primeros 5000 saltos.
## Contenido
Los archivos que se proporcionan son los siguientes:
1. **main.cpp**: El archivo principal donde se declaran las variables y ejecutan las funciones.
2. **tarea1.cpp**: Archivo que implementa las funciones necesarias.
3. **tarea1.h**: Archivo con los encabezados de las funciones implementas y con los includes necesarios para el programa.
4. **branch-trace-gcc.trace.gz**: Archivo comprimido con los saltos a procesar.
5. **branch**: Archivo binario del programa (Ejecutable).
6. **README.md**: Información general del programa.
7. **Makefile(opcional)**: Se puede utilizar para compilar y ejecutar el programa con parámetros por defecto.
# Compilación y Ejecución
Para compilar el programa se utiliza el siguiente comando:
- g++ -o branch main.cpp tarea1.cpp.

Para ejecutar el código es necesario saber los valores de los parámetros que éste recibe.

1. **-s**: Tamaño del arreglo para el predictor (2^s).
3. **-gh**: Cantidad de bits de historia del registro global.
2. **-bp**: Tipo de predictor (0 = "Bimodal", 1 = "Privado", 2 = "Global", 3 = "Torneo").
4. **-ph**: Cantidad de bits de historia en cada entrada de la tabla.
5. **-o**: Opción para realizar el archivo (1), cualquier otro valor no realiza el archivo. 

Un ejemplo de la ejecución sería el siguiente: 
- gunzip -c branch-trace-gcc.trace.gz | ./branch -s 3 -bp 3 -gh 4 -ph 3 -o 1

## Dependencias
Se debe contar con todos los archivos .cpp y .h, además del archivo comprimido en la misma carpeta al ejecutar el código.
