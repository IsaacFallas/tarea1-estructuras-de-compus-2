#include "tarea1.h"


using namespace std;

//Calcula el indice para el predictor bimodal, utiliza operadores a nivel de bit para calcular el indice.
//En este caso realiza una AND con el PC y el número más grande que se puede representar con la cantidad
//de bits del tamaño del arreglo(BHTmax), esto obtiene los últimos n - bits del PC y retorna el indice.
int Bimodal(long int PC, int BHTmax){

int indice = PC & BHTmax;

return indice;

}

//Calcula el indice del predictor global, utiliza operadores a nivel de bit para calcular el indice.
//Aquí se realiza una AND con el registro global y el valor máximo que se puede representar con la cantidad
//de bits del registro global, esto corta la cantidad de bits necesarios para realizar la XOR con el PC, seguidamente
//se realiza una AND con el número más grande que se puede representar con la cantidad de bits del tamaño del arreglo.
//Esto vuelve a cortar el resultado y devuelve el indice para el predictor global.
int Global(long int PC, int BHTmax, int GlobalResgisterMAX, int &RegistroGlobal){

int indice;

indice = RegistroGlobal & GlobalResgisterMAX;

indice = indice ^ PC;

indice = indice & BHTmax;

return indice;

}

//El indice para el predictor privado funciona de la misma forma que el predictor global, solamente que se generan en una tabla
//para la tabla se usa un vector que contiene los registros con la historia de cada salto, nuevamente se vuelven a realizar las
//mismas operaciones, la diferencia es que se obtiene el indice bimodal para obtener el registro de la tabla, al cual se le aplican
//las mismas operaciones que el indice global con la diferencia que se realiza con el número más grande que se puede representar con
//la cantidad de bits del parámetro ph 
int Private(long int PC, vector<int> &Table, int &indiceTable, int BHTmax, int PrivateResgisterMAX){

indiceTable = Bimodal(PC, BHTmax);

int indice;

indice = Table[indiceTable] & PrivateResgisterMAX;
indice = indice ^ PC;
indice = indice & BHTmax;

return indice;

}

//Función que se encarga de crear el predictor, retorna un puntero de tipo entero que inicializa sus valores en cero,
//el tamaño se calcula con el parámetro size, para ello se realiza 2^size.
int* CrearPredictor(int size){

size = exp((size) * log10(2) * log(10));

int* predictor = new int [size];

for (size_t i = 0; i < size; i++)
    {
        predictor[i] = 0;
    }

    return predictor;

}

//Función que crea la tabla para el predictor privado, en este caso retorna un vector de enteros inicializado en cero,
//el tamaño del arreglo se calcula de la misma forma que la función anterior, 2^BHT
vector<int> PrivateTable(int BHT){

    BHT = exp((BHT) * log10(2) * log(10));

    vector<int> PrivateTab;

    for (int i = 0; i < BHT; i++)
    {
        PrivateTab.push_back(0);
    }
    
return PrivateTab;

}

//Función que realiza la lógica para el predictor por torneo, dicha función recibe la estructura del metapredictor y la actualiza
//según los resultados obtenidos en los parámetros booleanos de privado y global, además recibe el indice para saber a que posición
//de la estructura del metapredictor debe ingresar
void Tournament(int metapredictor[], bool privado, bool global, int indice){

//Estados del metapredictor
int StronglyPrefGShare = 3;
int WeaklyPrefGShare = 2;
int WeaklyPrefPShare = 1;
int StronglyPrefPShare = 0;

//Condiciones para actualizar el metapredictor según los parámetros recibidos

       if (global == true && privado == false)
       {
          if (metapredictor[indice] == StronglyPrefPShare)
          {

           metapredictor[indice] = WeaklyPrefPShare;
              
          }else if (metapredictor[indice] == WeaklyPrefPShare)
          {

           metapredictor[indice] = WeaklyPrefGShare;   

          }else if (metapredictor[indice] == WeaklyPrefGShare)
          {
        
           metapredictor[indice] = StronglyPrefGShare;  
           

          }else if (metapredictor[indice] == StronglyPrefGShare)
          {

           metapredictor[indice] = StronglyPrefGShare;  

          }
          

       }else if (global == false && privado == true)
       {

          if (metapredictor[indice] == StronglyPrefPShare)
          {

           metapredictor[indice] = StronglyPrefPShare;
              
          }else if (metapredictor[indice] == WeaklyPrefPShare)
          {

           metapredictor[indice] = StronglyPrefPShare;   

          }else if (metapredictor[indice] == WeaklyPrefGShare)
          {
        
           metapredictor[indice] = WeaklyPrefPShare;  
           

          }else if (metapredictor[indice] == StronglyPrefGShare)
          {

           metapredictor[indice] = WeaklyPrefGShare;  

          }
           
       }
       
       

}

//Función que realiza la lógica para el predictor privado, esta recibe la estructura del predictor, la tabla de registros de historia
//de los saltos, el resultado del salto, el indice de la estructura del predictor a actualizar, el indice de la tabla de registros a actualizar
//y los demás parámetros son los datos que debe imprimir en consola sobre las predicciones realizadas, retorna verdadero si la predicción fue correcta
//o falso si fue incorrecta.
bool PredictorPrivado(int predictor[], vector<int> &Table, string state, int indice, int &indiceTable, int &NumBranch, int &CorrectTaken, int &IncorrectTaken, int &CorrectNotTaken, int &IncorrectNotTaken){

//Estados del predictor
    int StronglyTaken = 3;
    int WeaklyTaken = 2;
    int WeaklyNotTaken = 1;
    int StronglyNotTaken = 0;

    bool privado;

//Condiciones para actualizar el estado del predictor, además se actualizan los datos de los parámetros
        if ((predictor[indice] == StronglyNotTaken) && (state == "T"))
        {
           predictor[indice] = WeaklyNotTaken;
           Table[indiceTable] = Table[indiceTable] << 1; //Se desplaza a la izquierda
           Table[indiceTable] = Table[indiceTable] + 1; //Se introduce un uno en el registro de la tabla si el resultado es taken
           NumBranch++;
           IncorrectTaken++;
           privado = false;

        }else if ((predictor[indice] == StronglyNotTaken) && (state == "N"))
        {
            predictor[indice] = StronglyNotTaken;
            Table[indiceTable] = Table[indiceTable] << 1; //Se desplaza a la izquierda
            Table[indiceTable] = Table[indiceTable] + 0; //Se introduce un 0 en el registro de la tabla si el resultado es not taken
            NumBranch++;
            CorrectNotTaken++;
            privado = true;

        }else if ((predictor[indice] == WeaklyNotTaken) && (state == "T"))
        {
            predictor[indice] = WeaklyTaken;
            Table[indiceTable] = Table[indiceTable] << 1;
            Table[indiceTable] = Table[indiceTable] + 1;
            NumBranch++;
            IncorrectTaken++;
            privado = false;

        }else if ((predictor[indice] == WeaklyNotTaken) && (state == "N"))
        {
            predictor[indice] = StronglyNotTaken;
            Table[indiceTable] = Table[indiceTable] << 1;
            Table[indiceTable] = Table[indiceTable] + 0;
            NumBranch++;
            CorrectNotTaken++;
            privado = true;

        }else if ((predictor[indice] == WeaklyTaken) && (state == "T"))
        {
            predictor[indice] = StronglyTaken;
            Table[indiceTable] = Table[indiceTable] << 1;
            Table[indiceTable] = Table[indiceTable] + 1;
            NumBranch++;
            CorrectTaken++;
            privado = true;

        }else if ((predictor[indice] == WeaklyTaken) && (state == "N"))
        {
            predictor[indice] = WeaklyNotTaken;
            Table[indiceTable] = Table[indiceTable] << 1;
            Table[indiceTable] = Table[indiceTable] + 0;
            NumBranch++;
            IncorrectNotTaken++;
            privado = false;

        }else if ((predictor[indice] == StronglyTaken) && (state == "T"))
        {
            predictor[indice] = StronglyTaken;
            Table[indiceTable] = Table[indiceTable] << 1;
            Table[indiceTable] = Table[indiceTable] + 1;
            NumBranch++;
            CorrectTaken++;
            privado = true;

        }else if ((predictor[indice] == StronglyTaken) && (state == "N"))
        {
            predictor[indice] = WeaklyTaken;
            Table[indiceTable] = Table[indiceTable] << 1;
            Table[indiceTable] = Table[indiceTable] + 0;
            NumBranch++;
            IncorrectNotTaken++;
            privado = false;
        }

        return privado;

}


//Función que realiza la lógica para el predictor global, esta recibe la estructura del predictor, el registro global,
//el resultado del salto, el indice de la estructura del predictor a actualizar y los demás parámetros son los datos que debe
//imprimir en consola sobre las predicciones realizadas, retorna verdadero si la predicción fue correcta
//o falso si fue incorrecta..
bool PredictorGlobal(int predictor[], int &RegistroGlobal, string state, int indice, int &NumBranch, int &CorrectTaken, int &IncorrectTaken, int &CorrectNotTaken, int &IncorrectNotTaken){

//Estados del predictor
    int StronglyTaken = 3;
    int WeaklyTaken = 2;
    int WeaklyNotTaken = 1;
    int StronglyNotTaken = 0;

    //Condiciones para actualizar el estado del predictor, además se actualizan los datos de los parámetros
        if ((predictor[indice] == StronglyNotTaken) && (state == "T"))
        {
           predictor[indice] = WeaklyNotTaken;
           RegistroGlobal = RegistroGlobal << 1; //Desplazamiento a la izquierda
           RegistroGlobal = RegistroGlobal + 1; //Se ingresa un 1 si el resultado es taken
           NumBranch++;
           IncorrectTaken++;
           return false;

        }else if ((predictor[indice] == StronglyNotTaken) && (state == "N"))
        {
            predictor[indice] = StronglyNotTaken;
            RegistroGlobal = RegistroGlobal << 1; //Desplazamiento a la izquierda
            RegistroGlobal = RegistroGlobal + 0; //Se ingresa un 0 si el resultado es not taken
            NumBranch++;
            CorrectNotTaken++;
            return true;

        }else if ((predictor[indice] == WeaklyNotTaken) && (state == "T"))
        {
            predictor[indice] = WeaklyTaken;
            RegistroGlobal = RegistroGlobal << 1;
            RegistroGlobal = RegistroGlobal + 1;
            NumBranch++;
            IncorrectTaken++;
            return false;

        }else if ((predictor[indice] == WeaklyNotTaken) && (state == "N"))
        {
            predictor[indice] = StronglyNotTaken;
            RegistroGlobal = RegistroGlobal << 1;
            RegistroGlobal = RegistroGlobal + 0;
            NumBranch++;
            CorrectNotTaken++;
            return true;

        }else if ((predictor[indice] == WeaklyTaken) && (state == "T"))
        {
            predictor[indice] = StronglyTaken;
            RegistroGlobal = RegistroGlobal << 1;
            RegistroGlobal = RegistroGlobal + 1;
            NumBranch++;
            CorrectTaken++;
            return true;

        }else if ((predictor[indice] == WeaklyTaken) && (state == "N"))
        {
            predictor[indice] = WeaklyNotTaken;
            RegistroGlobal = RegistroGlobal << 1;
            RegistroGlobal = RegistroGlobal + 0;
            NumBranch++;
            IncorrectNotTaken++;
            return false;

        }else if ((predictor[indice] == StronglyTaken) && (state == "T"))
        {
            predictor[indice] = StronglyTaken;
            RegistroGlobal = RegistroGlobal << 1;
            RegistroGlobal = RegistroGlobal + 1;
            NumBranch++;
            CorrectTaken++;
            return true;

        }else if ((predictor[indice] == StronglyTaken) && (state == "N"))
        {
            predictor[indice] = WeaklyTaken;
            RegistroGlobal = RegistroGlobal << 1;
            RegistroGlobal = RegistroGlobal + 0;
            NumBranch++;
            IncorrectNotTaken++;
            return false;
        }

}


//Función que realiza la lógica para el predictor bimodal, esta recibe la estructura del predictor, el resultado del salto,
//el indice de la estructura del predictor a actualizar y los demás parámetros son los datos que debe imprimir en consola
//sobre las predicciones realizadas.
void PredictorBimodal(int predictor[], string state, int indice, int &NumBranch, int &CorrectTaken, int &IncorrectTaken, int &CorrectNotTaken, int &IncorrectNotTaken){

//Estados del predictor

    int StronglyTaken = 3;
    int WeaklyTaken = 2;
    int WeaklyNotTaken = 1;
    int StronglyNotTaken = 0;

//Condiciones para actualizar el predictor y los parámetros recibidos
        if ((predictor[indice] == StronglyNotTaken) && (state == "T"))
        {
           predictor[indice] = WeaklyNotTaken; 
           NumBranch++;
           IncorrectTaken++;

        }else if ((predictor[indice] == StronglyNotTaken) && (state == "N"))
        {
            predictor[indice] = StronglyNotTaken;
            NumBranch++;
            CorrectNotTaken++;

        }else if ((predictor[indice] == WeaklyNotTaken) && (state == "T"))
        {
            predictor[indice] = WeaklyTaken;
            NumBranch++;
            IncorrectTaken++;

        }else if ((predictor[indice] == WeaklyNotTaken) && (state == "N"))
        {
            predictor[indice] = StronglyNotTaken;
            NumBranch++;
            CorrectNotTaken++;

        }else if ((predictor[indice] == WeaklyTaken) && (state == "T"))
        {
            predictor[indice] = StronglyTaken;
            NumBranch++;
            CorrectTaken++;

        }else if ((predictor[indice] == WeaklyTaken) && (state == "N"))
        {
            predictor[indice] = WeaklyNotTaken;
            NumBranch++;
            IncorrectNotTaken++;

        }else if ((predictor[indice] == StronglyTaken) && (state == "T"))
        {
            predictor[indice] = StronglyTaken;
            NumBranch++;
            CorrectTaken++;

        }else if ((predictor[indice] == StronglyTaken) && (state == "N"))
        {
            predictor[indice] = WeaklyTaken;
            NumBranch++;
            IncorrectNotTaken++;
        }
    

}

//Función que retorna el porcentaje de las predicciones correctas, recibe el número de branches, y las predicciones correctas de los saltos
//que se tomaron y no se tomaron.
float Percentage(int &NumBranch, int &CorrectTaken, int &CorrectNotTaken){

    return (((float)(CorrectTaken + CorrectNotTaken)/(float)(NumBranch))*(float)(100));

}

//Función que escribe en el archivo que se recibe por parámetro, recibe la estructura del predictor, el indice del predictor,
//el PC y el resultado del salto.
void CrearArchivo(ofstream &archivo, int predictor[], int indice, long int PC, string state){

//Estados del predictor
    int StronglyTaken = 3;
    int WeaklyTaken = 2;
    int WeaklyNotTaken = 1;
    int StronglyNotTaken = 0;

//Condiciones para saber que escribir en el archivo
        if ((predictor[indice] == StronglyNotTaken) && (state == "T"))
        {

        archivo << PC << "  " << state << "         " << "N" << "           " << "incorrect" << endl;


        }else if ((predictor[indice] == StronglyNotTaken) && (state == "N"))
        {
        
        archivo << PC << "  " << state << "         " << "N" << "           " << "correct" << endl;

            
        }else if ((predictor[indice] == WeaklyNotTaken) && (state == "T"))
        {

        archivo << PC << "  " << state << "         " << "N" << "           " << "incorrect" << endl;
            
        }else if ((predictor[indice] == WeaklyNotTaken) && (state == "N"))
        {
           
        archivo << PC << "  " << state << "         " << "N" << "           " << "correct" << endl;

        }else if ((predictor[indice] == WeaklyTaken) && (state == "T"))
        {
            
        archivo << PC << "  " << state << "         " << "T" << "           " << "correct" << endl;

        }else if ((predictor[indice] == WeaklyTaken) && (state == "N"))
        {
            
        archivo << PC << "  " << state << "         " << "T" << "           " << "incorrect" << endl;

        }else if ((predictor[indice] == StronglyTaken) && (state == "T"))
        {
           
        archivo << PC << "  " << state << "         " << "T" << "           " << "correct" << endl;

        }else if ((predictor[indice] == StronglyTaken) && (state == "N"))
        {
            
        archivo << PC << "  " << state << "         " << "T" << "           " << "incorrect" << endl;

        }

}

//Función que imprime en consola los parámetros solicitados
void DisplayInfo(int tipo, int BHT, int GlobalRegister, int PrivateRegister, int NumBranch, int CorrectTaken, int IncorrectTaken, int CorrectNotTaken, int IncorrectNotTaken, float PercentageCorrect){

string predictor;

//Condiciones para imprimir el nombre del predictor utilizado

if (tipo == 0)
{
    predictor = "Bimodal";

}else if (tipo == 1)
{
    predictor = "PShare";

}else if (tipo == 2)
{
    predictor = "GShare";

}else if (tipo == 3)
{
    predictor = "Tournament";
}

cout << "-----------------------------------------------------------------" << endl;
cout << "Prediction parameters:" << endl;
cout << "-----------------------------------------------------------------" << endl;
cout << "Branch prediction type:" << "                               " << predictor << endl;
cout << "BHT size (entries):" << "                                   " << BHT << endl;
cout << "Global history register size:" << "                         " << GlobalRegister << endl;
cout << "Private history register size:" << "                        " << PrivateRegister << endl;
cout << "-----------------------------------------------------------------" << endl;
cout << "Simulation results:" << endl;
cout << "-----------------------------------------------------------------" << endl;
cout << "Number of branch:" << "                                     " << NumBranch << endl;
cout << "Number of correct prediction of taken branches:" << "       " << CorrectTaken << endl;
cout << "Number of incorrect prediction of taken branches:" << "     " << IncorrectTaken << endl;
cout << "Correct prediction of not taken branches:" << "             " << CorrectNotTaken << endl;
cout << "Incorrect prediction of not taken branches:" << "           " << IncorrectNotTaken << endl;
cout << "Percentage of correct predictions:" << "                    " << PercentageCorrect << endl;
cout << "-----------------------------------------------------------------" << endl;

}


