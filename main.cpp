#include "tarea1.h"

using namespace std;

int main(int argc, char* argv[]){

//Parámetros
int tipo = stoi(argv[4]); 
int BHT = stoi(argv[2]); 
int GlobalRegister = stoi(argv[6]); 
int PrivateRegister = stoi(argv[8]);
int File = stoi(argv[10]);


//Valores máximos con los parámetros, ejemplo: (X^Y - 1)
//Estos valores se usan en la lógica para calcular los índices de los predictores
int BHTmax = (exp(BHT * log10(2) * log(10))) - 1;
int GlobalResgisterMAX = (exp(GlobalRegister * log10(2) * log(10))) - 1;
int PrivateResgisterMAX = (exp(PrivateRegister * log10(2) * log(10))) - 1;


//Archivo del predictor
ofstream archivo;

//Condicion para crear el archivo
if (File == 1)
{
    string fileName;

    //Condiciones para escribir el nombre del archivo
    if (tipo == 0)
    {

    fileName = "Bimodal";

    }else if (tipo == 1)
    {

    fileName = "PShare";

    }else if (tipo == 2)
    {

    fileName = "GShare";

    }else if (tipo == 3)
    {

    fileName = "Tournament";

    }

    //Encabezado del archivo
    archivo.open(fileName, ios::out); 
    archivo << "PC" << "          " << "Outcome" << "   " << "Prediction" << "  " << "correct/incorrect" << endl;
}

//Variables para el PC y el resultado del salto que se encuentran en el archivo
long int PC;
string state;

//Variable que lleva el registro global para el predictor global, la idea es trabajar la variable a nivel de bit
//sin pasarla a binario, dicha lógica se encuentra en la función del predictor global y del indice del predictor global
int RegGlobal = 0;

//El indice de tabla se crea para el predictor privado, obtiene el indice de la tabla para obtener el registro de historia
//usando los últimos n-bits del PC, dicho valor se trabaja a nivel de bit, dicha lógica se realiza en la función que calcula
//el indice del predictor privado
int indiceTable = 0;

//Se crea un vector que representa la tabla de registros de historia del predictor privado, dichos registros se trabajan
//a nivel de bit al igual que el "RegGlobal", dicha lógica se encuentra en la función del predictor privado y el indice privado
vector<int> PrivateTab = PrivateTable(BHT);

    
//Se declaran los predictores
int* predictor;
int* predictorP;
int* predictorG;
int* metapredictor;

//Resultados que se imprimen en pantalla para los predictores
int NumBranch = 0;
int CorrectTaken = 0; 
int IncorrectTaken = 0;
int CorrectNotTaken = 0;
int IncorrectNotTaken = 0;  
float PercentageCorrect;

//Variables booleanas para los predictores global y privado, se usan en el predictor por torneo para poder actualizar el 
//metapredictor
bool global;
bool privado;

//Variables para almacenar los indices de cada predictor
int indiceBimodal;
int indiceGlobal;
int indicePrivado;


//Crea los predictores necesarios
predictor = CrearPredictor(BHT);

predictorP = CrearPredictor(BHT);

predictorG = CrearPredictor(BHT);

metapredictor = CrearPredictor(BHT);


//Variable que se pasa por referencia a los predictores que no contabilizan resultados en el predictor por torneo
//Solo se contabilizan los que el metapredictor elige
int zero = 0;

//Contador para contar las primeras 5000 instrucciones
int cnt = 0; 

cin >> PC;
//Ciclo while que lee el archivo hasta que se finaliza
while (!cin.eof())
{
    //Obtiene los valores del PC y el resultado del archivo

    cin >> state;

    //Secuencia de ifs para utilizar el predictor escogido
    //Elige el predictor bimodal
    if (tipo == 0)
    {
       //Función que calcula el indice bimodal
       indiceBimodal = Bimodal(PC, BHTmax);
        
        //Condición para escribir en el archivo para las primeras 5000 instrucciones
       if (File == 1 && cnt < 5000)
       {
        //Función que escribe en el archivo la predicción hecha por el salto
        CrearArchivo(archivo, predictor, indiceBimodal, PC, state);
        cnt++;

       }
        //Función que realiza la lógica para el predictor bimodal
       PredictorBimodal(predictor, state, indiceBimodal, NumBranch, CorrectTaken, IncorrectTaken, CorrectNotTaken, IncorrectNotTaken);
    

    }else if (tipo == 2) //Elige el predictor global
    {
        //Función que calcula el indice global
       indiceGlobal = Global(PC, BHTmax, GlobalResgisterMAX, RegGlobal);

        //Condición para escribir en el archivo para las primeras 5000 instrucciones
        if (File == 1 && cnt < 5000)
        {
        //Función que escribe en el archivo la predicción hecha por el salto
        CrearArchivo(archivo, predictor, indiceGlobal, PC, state);
        cnt++;

        }
        //Función que realiza la lógica del predictor global
       global = PredictorGlobal(predictor, RegGlobal, state, indiceGlobal, NumBranch, CorrectTaken, IncorrectTaken, CorrectNotTaken, IncorrectNotTaken);
        

    }else if (tipo == 1)//Elige el predictor privado
    {
      //Función que calcula el indice privado
       indicePrivado = Private(PC, PrivateTab, indiceTable, BHTmax, PrivateResgisterMAX);

        //Condición para escribir en el archivo para las primeras 5000 instrucciones
        if (File == 1 && cnt < 5000)
        {
        //Función que escribe en el archivo la predicción hecha por el salto
        CrearArchivo(archivo, predictor, indicePrivado, PC, state);
        cnt++;

        }
        //Función que realiza la lógica del predictor privado
       privado = PredictorPrivado(predictor, PrivateTab, state, indicePrivado, indiceTable, NumBranch, CorrectTaken, IncorrectTaken, CorrectNotTaken, IncorrectNotTaken); 


    }else if (tipo == 3)//Elige el predictor por torneo
    {
        //Función que calcula el indice bimodal para indexar el metapredictor
        indiceBimodal = Bimodal(PC, BHTmax);

        //Condición para usar el predictor privado mediante el metapredictor "0" o "1" están asociados al predictor privado
        if (metapredictor[indiceBimodal] == 0 || metapredictor[indiceBimodal] == 1)
        {

        //Se deben usar ambos predictores a pesar de que se elija el privado, sin embargo solo se contabilizan los resultados de
        //de este.
        //Función que obiene el indice global
        indiceGlobal = Global(PC, BHTmax, GlobalResgisterMAX, RegGlobal);

        //Se realiza la lógica del predictor global, en este caso si se utiliza el valor booleano para actualizar el metapredictor
        global = PredictorGlobal(predictorG, RegGlobal, state, indiceGlobal, zero, zero, zero, zero, zero);

        //Función que obtiene el indice privado
        indicePrivado = Private(PC, PrivateTab, indiceTable, BHTmax, PrivateResgisterMAX);

        //Condición para escribir en el archivo para las primeras 5000 instrucciones
        if (File == 1 && cnt < 5000)
        {
        //Función que escribe en el archivo la predicción hecha por el salto
        CrearArchivo(archivo, predictorP, indicePrivado, PC, state);
        cnt++;

        }
        
        //Función que realiza la lógica del predictor privado y devuelve el resultado del salto en el booleano para el metapredictor
        privado = PredictorPrivado(predictorP, PrivateTab, state, indicePrivado, indiceTable, NumBranch, CorrectTaken, IncorrectTaken, CorrectNotTaken, IncorrectNotTaken); 
        
        //Función que realiza la lógica del metapredictor con las predicciones de los predictores anteriores
        Tournament(metapredictor, privado, global, indiceBimodal);

        
        //Condición para usar el predictor global mediante el metapredictor "2" o "3" están asociados al predictor global
        }else if (metapredictor[indiceBimodal] == 2 || metapredictor[indiceBimodal] == 3)
        {
        //Se deben usar ambos predictores a pesar de que se elija el global, sin embargo solo se contabilizan los resultados de
        //de este.
        //Función que obiene el indice privado
        indicePrivado = Private(PC, PrivateTab, indiceTable, BHTmax, PrivateResgisterMAX);

        //Función que realiza la lógica del predictor privado y devuelve el resultado del salto en el booleano para el metapredictor    
        privado = PredictorPrivado(predictorP, PrivateTab, state, indicePrivado, indiceTable, zero, zero, zero, zero, zero); 

        //Función que obiene el indice global
        indiceGlobal = Global(PC, BHTmax, GlobalResgisterMAX, RegGlobal);

        //Condición para escribir en el archivo para las primeras 5000 instrucciones
        if (File == 1 && cnt < 5000)
        {
        //Función que escribe en el archivo la predicción hecha por el salto
        CrearArchivo(archivo, predictorG, indiceGlobal, PC, state);
        cnt++;

        }
        //Se realiza la lógica del predictor global, en este caso si se utiliza el valor booleano para actualizar el metapredictor
        global = PredictorGlobal(predictorG, RegGlobal, state, indiceGlobal, NumBranch, CorrectTaken, IncorrectTaken, CorrectNotTaken, IncorrectNotTaken);

        //Función que realiza la lógica del metapredictor con las predicciones de los predictores anteriores
        Tournament(metapredictor, privado, global, indiceBimodal);

        
        }
        
    }
    cin >> PC;

}

    //Función que calcula el procentaje de predicciones correctas
    PercentageCorrect =  Percentage(NumBranch, CorrectTaken, CorrectNotTaken);
    
    //Función que imprime en consola los resultados del predictor
    DisplayInfo(tipo, BHTmax+1, GlobalRegister, PrivateRegister, NumBranch, CorrectTaken, IncorrectTaken, CorrectNotTaken, IncorrectNotTaken, PercentageCorrect);



return 0;

}



